"use strict";

const { VueLoaderPlugin } = require("vue-loader");

module.exports = {
  mode: "development",
  entry: ["./src/index.ts"],
  output: {
    library: "vButton",
    libraryTarget: "umd",
    filename: "vButton.js"
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        use: "vue-loader"
      },
      {
        test: /\.scss$/,
        use: ["vue-style-loader", "css-loader", "sass-loader"]
      }
    ]
  },
  plugins: [new VueLoaderPlugin()]
};
